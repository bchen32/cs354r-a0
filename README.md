# cs354r-a0

Running `AsteroidsWhoo.x86_64` from this folder should work on Linux, although it wasn't tested on the lab machines, due to the executable being optional.

If the above doesn't work, open the `Assignment0` project in Godot and press `F5` to run the project