using Godot;
using System;

public partial class Main : Node2D
{
	public Player Player;
	public int Frame;
	public PackedScene Asteroid;
	public Label Score;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Player = GetNode<Player>("Player");
		Score = GetNode<Label>("Control/Label");
		Frame = 0;
		Asteroid = GD.Load<PackedScene>("res://Asteroid.tscn");
	}
	
	public override void _PhysicsProcess(double delta)
	{
		if (Frame == 30) {
			Frame = 0;
			Asteroid asteroid = (Asteroid) Asteroid.Instantiate();
			double theta = GD.RandRange(0.0f, Math.PI);
			Vector2 pos = new Vector2((float) (Mathf.Cos(theta) * 400.0d), (float) (Mathf.Sin(theta) * 400.0d));
			pos.X += 160;
			pos.Y += 120;
			asteroid.Position = pos;
			double var = GD.RandRange(-Math.PI / 6, Math.PI / 6);
			double velTheta = theta + Math.PI + var;
			asteroid.Velocity = new Vector2((float) (Mathf.Cos(velTheta) * 100.0d), (float) (Mathf.Sin(velTheta) * 100.0d));
			AddChild(asteroid);
		}
		Frame++;
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
		if (Player.Dead) {
			Control lose = GetNode<Control>("Control/Lose");
			lose.Visible = true;
		}
		Score.Text = "Score: " + Player.Kills;
	}
}
