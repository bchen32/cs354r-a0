using Godot;
using System;

public partial class Title : Control
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Button startButton = GetNode<Button>("VBoxContainer/StartButton");
		startButton.Pressed += _OnStartPressed;
		Button quitButton = GetNode<Button>("VBoxContainer/QuitButton");
		quitButton.Pressed += _OnQuitPressed;
	}

	public void _OnStartPressed()
	{
		GetTree().ChangeSceneToFile("res://Main.tscn");
	}
	
	public void _OnQuitPressed()
	{
		GetTree().Quit();
	}
}
