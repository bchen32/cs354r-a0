using Godot;
using System;

public partial class Lose : Control
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		Button RetryButton = GetNode<Button>("VBoxContainer/Button");
		RetryButton.Pressed += _OnPressed;
	}
	
	public void _OnPressed() {
		GetTree().ReloadCurrentScene();
	}
}
