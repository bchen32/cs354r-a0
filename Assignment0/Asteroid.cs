using Godot;
using System;

public partial class Asteroid : Area2D
{
	public Vector2 Velocity;
	public bool dead;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		dead = false;
		BodyEntered += _OnEntered;
	}
	
	public void _OnEntered(Node2D body) {
		if (body is Player) {
			Player player = (Player) body;
			player.Dead = true;
		}
	}
	
	public override void _PhysicsProcess(double delta)
	{
		if (dead) {
			QueueFree();
		}
		Position += Velocity * (float) delta;
	}
}
