using Godot;
using System;

public partial class Player : CharacterBody2D
{
	public const float Accel = 100.0f;
	public const float TurnVel = 5.0f;
	
	public double AngularVelocity = 0.0d;
	
	public Sprite2D Laser;
	public Area2D LaserHitbox;
	public AudioStreamPlayer Sound;
	public int Frame;
	public bool Dead;
	public int Kills;
	
	public override void _Ready()
	{
		Laser = GetNode<Sprite2D>("Laser");
		LaserHitbox = GetNode<Area2D>("Area2D");
		Sound = GetNode<AudioStreamPlayer>("AudioStreamPlayer");
		LaserHitbox.AreaEntered += _OnEntered;
		Frame = 0;
		Kills = 0;
		LaserHitbox.SetCollisionMaskValue(2, false);
		Velocity = new Vector2((float) GD.RandRange(-20.0f, 20.0f), (float) GD.RandRange(-20.0f, 20.0f));
	}
	
		public void _OnEntered(Area2D body) {
			if (body is Asteroid) {
				Asteroid asteroid = (Asteroid) body;
				asteroid.dead = true;
				Kills++;
			}
		}

	public override void _PhysicsProcess(double delta)
	{
		if (Dead) {
			QueueFree();
		}
		if (Frame > 20) {
			Laser.Visible = false;
			LaserHitbox.SetCollisionMaskValue(2, false);
		}
		Vector2 vel = Velocity;
		int turnDir = 0;
		if (Input.IsActionPressed("ui_left")) {
			turnDir--;
		}
		if (Input.IsActionPressed("ui_right")) {
			turnDir++;
		}
		if (Laser.Visible) {
			turnDir = 0;
		}
		AngularVelocity = TurnVel * turnDir;
		Rotate((float) (AngularVelocity * delta));
		if (Input.IsActionPressed("ui_up")) {
			vel += Accel * (float) delta * -Transform.Y;
		}
		if (Input.IsActionJustPressed("ui_accept") && Frame > 40) {
			Frame = 0;
			Laser.Visible = true;
			LaserHitbox.SetCollisionMaskValue(2, true);
			Sound.Play();
		}
		Velocity = vel;
		MoveAndSlide();
		Frame++;
	}
}
